
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package misclases;

/**
 *
 * @author Virgen
 */
public class Terreno {
    
    private int numLote;
    private float ancho;
    private float largo;
    
    //CONSTRUCTORES

    public Terreno() {
        this.numLote=0;
        this.largo=0.0f;
        this.ancho=0.0f;
    }

    public Terreno(int numLote, float ancho, float alto) {
        this.numLote = numLote;
        this.ancho = ancho;
        this.largo = largo;
    }
    
    public Terreno(Terreno t) {
        this.numLote = t.numLote;
        this.ancho = t.ancho;
        this.largo = t.largo;
    }
    
    //ENCAPSULADO
    public int getNumLote() {
        return numLote;
    }

    public void setNumLote(int numLote) {
        this.numLote = numLote;
    }

    public float getAncho() {
        return ancho;
    }

    public void setAncho(float ancho) {
        this.ancho = ancho;
    }

    public float getLargo() {
        return largo;
    }

    public void setLargo(float largo) {
        this.largo = largo;
    }
    
    
    //METODOS DE COMPORTAMIENTO
    
    public float calcularPerimetro(){
        return this.ancho * 2 + this.largo;
    }
    
    public float calcularArea(){
        return this.ancho * this.largo;
    }
    
    
}
